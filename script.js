var page1 = [
    ['Resp1', '1'],
    ['Resp2', '2'],
    ['Resp3', '3'],
    ['Resp4', '4'],
    ['Resp5', '5'],
    ['Resp6', '6']
];

function clickFail(num) {
    element = document.getElementById('fail' + num);
    element.classList.add('fail');
}

function clickSelect(num) {
    element = document.getElementById('respTable');
    if (element == null) {
        alert("Cannot find table!");
    }
    element.rows[num - 1].cells[0].innerHTML = page1[num - 1][0];
    element.rows[num - 1].cells[1].innerHTML = page1[num - 1][1];
}